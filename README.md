# Signature Based Bag Of Visual Words


Image Processing - SCC5830

Final Project

Author:
* Endi Daniel Coelho Silva

USP Number:
* 11548700

Folders and files:
* [Submission](./submission/) contains the Python code, and the notebook demo.
* [CBIR Images](/submission/cbirImages) dataset of images
* [Test Images](/submission/testImages) dataset of test images
* [Images](/images) contains images 
* https://youtu.be/cX8J9FL8aZo (Link to the video)
* bovw.py is the code from the BOF implemented in the https://github.com/maponti/imageprocessing_course_icmc/blob/master/07c_bag_of_features.ipynb this code was used just to make comparisons.
* sbovw.py is the code implemented in this work.
---------------------
Abstract:

* Signature Based Bag of Visual Words - (SBoVW) is a class of methods that dont need a visual dictionary. In the SBoVW methods the images are divided into blocks and signatures(feature vectors) are created to represent the content of each block as a visual word. Thus the objective of this project is generate a new methodology that can be used to describe the content of each block generating a signature (visual word). 

---------------------
Objective:

* The main objective of this project is build a new function that generate visual signatures in Signature based Bag of Visual World methods. 
---------------------
Image Processing Tasks:

* Image Segmentation
* Texture Analysis 
---------------------
Related Works:

* The Bag of Super Pixel Signatures - (BOSS) developed by Chino et al (2018) is a example of a SBoVW method, the blocks are obtained using SuperPixels and each SuperPixel is represented by visual signature observing the most frequent color values of the pixels.
---------------------
Proposal:

* The BOSS method will be used as base for this project, the image will be divided in blocks,  and in this project will be developed a new methodology to generate the visual signature for each block. 
---------------------
Steps to reach the objective:

* Divide image in blocks.

The images will be divided into blocks using two methodologies. 1-Using Super pixels. 2-Using blocks of fixed size. (In this case 8x8)

* Map each block to a visual word(feature vector).

1-The first uses complex networks to analyze the texture of the block. A graph is assembled so that each pixel is considered a vertex, an edge with weight proportional to the distance and absolute difference in gray levels is added for each pair of vertices. Derived networks are created keeping only those edges that has wight below to previously defined thresholds. The mean and entropy of the degree histogram of each of these networks are concatenated into a feature vector to form the visual word that will describe the block.

2-The second approach, generates a histogram for each block. From this histogram a vector formed by the mean and entropy of the histogram is used to represent the block as a visual word.

* Count the occurences of each visual word in the image to generate the signature of the image.

In this step, the number of visual words that occurred in the image are counted to form the histogram of visual words.

---------------------
Four approaches will be tested in this project.

* 1 - Signature Based Bag of Visual Word with Super Pixel and Firts order Statisticas of the Histogram - (SBoVW SPH), the image is divided in blocks using superpixels. For each superpixel is obtained a histogram H, the entropy and mean of the histogram are used to generate a feature vector and represent the block as a visual word.

* 2 - Signature Based Bag of Visual Word with Blocks and Firts order Statisticas of the Histogram - (SBoVW BH), the image is divided using 8x8 blocks. For each block is obtained a histogram H, the entropy and mean of the histogram are used to generate a feature vector and represent the block as a visual word.

The image below ilustrate this process:

![IMAGEM](images/estatistico.png)

* 3 - Signature Based Bag of Visual Word with Super Pixel and Complex Networks - (SBoVW SPRC), the image is divided in blocks using superpixels. Each superpixel is mapped to a complex network, the entropy and mean of the degree histogram( histogram of the number of edges in each vertex ) are used form a feature vector that represent the block as a visual word.

* 4 - Signature Based Bag of Visual Word with Blocks and Complex Networks - (SBoVW BRC), the image is divided using 8x8 blocks. Each block is mapped to a complex network, the entropy and mean of the degree histogram (histogram of the number of edges in each vertex) are used form a feature vector that represent the block as a visual word.

The image below ilustrate this process:

![IMAGEM](images/ComplexNetworks.png)
---------------------
Description Input Images:

* The utilized images are from the Corel10K dataset, but just a small size of the database will be used for tests. Will be used just two categories of the dataset. The images has size 192x128 or 128x192.

link to the dataset: http://www.ci.gxnu.edu.cn/cbir/Dataset.aspx

Two classes with 22 images were used. You can see the images in the folder: [BASE DE IMAGENS](/submission/cbirImages)

22 images from the class "ursos":

![IMAGEM](images/ursos.png)

22 images from the class "janelas":

![IMAGEM](images/janelas.png)
---------------------
Tests and Results:

To test the quality of the representation generated by each of the approaches, including the Bag of Features seen in class, a CBIR system was used. Considering that each class has 22 images, a search in the system is performed asking for the 22 most similar images, it is expected that the system returns the 22 images of the class belonging to the query image.

---------------------
Configuration: 

SBoVW SPH and SBoVW SPRC used 250 Super Pixels

SBoVW BH and SBoVW BRC used blocks of size 8x8

BOF = 150 visual words

The table below show the % of correct images returned in the CBIR system for each one of the approaches in the [test images](/submission/testImages) from the class janela.

|            | janela_01 | janela_03 | janela_05 | janela_07 | janela_09 |  Mean |
|:----------:|:---------:|:---------:|:---------:|:---------:|:---------:|:-----:|
|  SBoVW SPH |    90%    |    86%    |    86%    |    95%    |    86%    |  88%  |
| SBoVW SPRC |    77%    |    81%    |    59%    |    95%    |    72%    |  76%  |
|  SBoVW BH  |    90%    |    90%    |    86%    |    90%    |    81%    |  87%  |
|  SBoVW BRC |    63%    |    72%    |    59%    |    86%    |    77%    |  71%  |
|     BOF    |    77%    |    77%    |    72%    |    63%    |    81%    |  74%  |

The table below show the % of correct images returned in the CBIR system for each one of the approaches in the [test images](/submission/testImages) from the class urso.

|            | urso_01 | urso_03 | urso_05 | urso_07 | urso_09 | Mean |
|:----------:|:-------:|:-------:|:-------:|:-------:|:-------:|:----:|
|  SBoVW SPH |   72%   |   77%   |   90%   |   100%  |   100%  |  87% |
| SBoVW SPRC |   95%   |   86%   |   77%   |   90%   |   90%   |  87% |
|  SBoVW BH  |   27%   |   50%   |   100%  |   90%   |   81%   |  69% |
|  SBoVW BRC |   81%   |   72%   |   81%   |   72%   |   77%   |  76% |
|     BOF    |   81%   |   68%   |   63%   |   60%   |   77%   |  70% |

The table below show the overall results:

|            | janela | urso |
|:----------:|:------:|:----:|
|  SBoVW SPH |   88%  |  87% |
| SBoVW SPCN |   76%  |  87% |
|  SBoVW BH  |   87%  |  69% |
|  SBoVW BCN |   71%  |  76% |
|     BOF    |   74%  |  70% |

References:

Boss: Image retrieval using bag-of-superpixels signatures; DYT Chino, LC Scabora, C Traina Jr, AJM Traina; Proceedings of the 33rd Annual ACM Symposium on Applied Computing, 309-312




