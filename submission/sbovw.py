import copy
import glob
import math
from collections import Counter
import cv2
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from skimage import io
from skimage.feature import local_binary_pattern
from skimage.measure import regionprops
from skimage.segmentation import mark_boundaries, slic
from skimage.util import img_as_float

NSP  = 250
n_intensidades = 256
MODO = 2
# 1 para SUPERPIXELS, 2 PARA BLOCOS
SPB = 1
raios = [3]
imagens = []
diretorio = "cbirImages/"

limiares = [0.100,0.200]

#if plot = 1 then it shows the image with superpixels
def geraSuperPixels(imagem,numeroDeSuperPixels,plota):
    if(len(imagem.shape) == 3):
        #Run SuperPixel algorithm for color images
        segmentos = slic(imagem, n_segments = numeroDeSuperPixels, sigma = 5)
    else:
        #for grayscale images
        segmentos = slic(imagem, n_segments = numeroDeSuperPixels, compactness=0.1, sigma = 5)
    imagem = cv2.cvtColor(imagem, cv2.COLOR_BGR2RGB)
    #Plot image with marked SuperPixels
    if(plota == 1):
        fig = plt.figure("SuperPixels")
        ax = fig.add_subplot(1, 1, 1)
        ax.imshow(mark_boundaries(imagem, segmentos))
        plt.axis("off")
        plt.show()

    return segmentos


def geraBlocos(imagem,numeroDeBlocos):
    if(len(imagem.shape) == 3):
        gray = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
    else:
        gray = np.zeros(imagem.shape)
    windowsize_r = 8
    windowsize_c = 8
    contador=0
    for r in range(0,gray.shape[0] - windowsize_r, windowsize_r):
        for c in range(0,gray.shape[0] - windowsize_c, windowsize_c):
            gray[r:r+windowsize_r,c:c+windowsize_c] = contador
            contador+=1
    return gray 
##### FIRST ORDER STASTICS OF HISTOGRAM ####

# GENERATE THE HISTOGRAM OF COMPLEX NETWORKS #

def calculaHistogramaDoGrau(grafo,maxDegree):
    histograma = np.zeros(maxDegree)
    for vertice in grafo:
        grau = np.size(grafo[vertice][0])
        histograma[grau]+=1
    
    return histograma

# GENERATE THE HISTOGRAM OF INTENSITY LEVEL'S OR TEXTURE VALUES (LBP) # 

def geraHistogramaIntensidades(imagem,posistions):
    histograma = np.zeros(n_intensidades).astype(int)
    intensidades = imagem[posistions]
    for i in np.arange(n_intensidades):
        pixel_value_i = np.where(intensidades == i)
        histograma[i] = pixel_value_i[0].shape[0]
    return histograma

#CALCULATES DEGREE I PROBABILITY OCCURS IN THE HISTOGRAM
def calculaDensProbab(histograma,maxDegree):
    densProbab = np.zeros(maxDegree)
    densProbab =   histograma / (np.sum(histograma))
    return densProbab


# MEAN OF HISTOGRAM
def calculaMediaHistograma(densProbab,maxDegree):
    media = 0
    indices = np.arange(start=0, stop=maxDegree, step=1)
    prob = np.copy(densProbab)
    media = np.sum((indices * prob))
    return media

# ENTROPY
def calculaEntropia(densProbab,maxDegree):
   
    posistions = np.where(densProbab > 0)
    prob = np.copy(densProbab[posistions])
    logProb = np.log2(prob) 
    entropia = np.sum(prob*logProb)
    entropia = entropia*-1

    return(entropia)

# 
def calculaContraste(densProbab,maxDegree):
    contraste = 0
    indices = np.arange(start=0, stop=maxDegree, step=1)
   
    prob = np.copy(densProbab)
    contraste = np.sum(((indices* indices) * prob))

    return contraste

# ENERGY
def calculaEnergia(densProbab,maxDegree):
    energia = 0
    prob = np.copy(densProbab)
    energia = np.sum((prob*prob))
    return energia

#### END FIRST ORDER STATISTICS HISTOGRAM ####


#### BEGIN GENERATE VISUAL WORD FOR A SUPERPIXEL ####


#METHOD USING FIRST ORDER STATISTICS FROM THE HISTOGRAM OF INTENSITIES OR TEXTURE (LBP) 
def calculaAssEst(imagem,posistions):
    histograma = geraHistogramaIntensidades(imagem,posistions)
    representacao  = []
    dens = calculaDensProbab(histograma,n_intensidades)
    media = calculaMediaHistograma(dens,n_intensidades)
    entropia = calculaEnergia(dens,n_intensidades)
    energia = calculaEnergia(dens,n_intensidades)
    contraste = calculaContraste(dens,n_intensidades)
    representacao.append(int(media))
    #representacao.append(int(entropia))
    #representacao.append(int(energia))
    representacao.append(round(entropia, 1))
    #representacao.append(round(energia, 1))
    #representacao.append(int(contraste))
    return representacao

def geraPalavraVisualEstatisticaHist(superPixel,segmentos,imagem):
    
    mask = np.zeros(imagem.shape[:2], dtype = "uint8")
    mask[segmentos == superPixel] = 255
    positions =  np.where(mask==255)
    palavraVisual = calculaAssEst(imagem,positions)
 
    return palavraVisual

#METHOD USING FIRST ORDER STATISTICS FROM THE HISTOGRAM OF DEGRESS (COMPLEX NETWORK)
def geraPalavraVisualRedeComplexa2(superPixel,segmentos,imagem):
    mask = np.zeros(imagem.shape[:2], dtype = "uint8")
    mask[segmentos == superPixel] = 255
    #position of the pixels in the block
    positions =  np.where(mask==255)
    grafo = {}
    representacao = []

    for raio in raios:

        grafo,maxDegree = criaGrafoVizinhanca(positions,imagem,raio)
        if grafo == {}:
            break
        for limiar in limiares:
            grafoPodado = copy.deepcopy(grafo)
            for vertice in grafoPodado:
                array = np.array(grafoPodado[vertice][1])
                positions = np.where(array > limiar)
                grafoPodado[vertice][1] = np.delete(grafoPodado[vertice][1], positions) 
                grafoPodado[vertice][0] = np.delete(grafoPodado[vertice][0], positions) 
                    
            histograma = calculaHistogramaDoGrau(grafoPodado,maxDegree)
            distProbabilidade = calculaDensProbab(histograma,maxDegree)
            media = calculaMediaHistograma(distProbabilidade,maxDegree)
            entropia = calculaEntropia(distProbabilidade,maxDegree)
            energia = calculaEnergia(distProbabilidade,maxDegree)
            contraste = calculaContraste(distProbabilidade,maxDegree)
            #representacao.append(round(media, 1))
            #representacao.append(round(entropia,1))
            representacao.append(int(media))
            representacao.append(int(entropia))
    return representacao

#### END GENERATE VISUAL WORD FOR A SUPERPIXEL ####


#### GENERATE COMPLEX NETWORK #### 

#versao final
def criaGrafoVizinhanca(positions,imagem,raio):
    grafo = {}
    numPixeisSp = positions[0].size
    centro = [sum(positions[0]) // numPixeisSp, sum(positions[1] // numPixeisSp)]
    limites = imagem.shape
    #ensures that the center will be in a location where the neighborhood can be obtained
    if(centro[0] <= raio):
        centro[0] = centro[0] + raio
    if(centro[1] <= raio):
        centro[1] = centro[1] + raio
    
    if(abs(centro[0] - limites[0]) <= raio):
        centro[0] = centro[0] - raio
    if(abs(centro[1] - limites[1]) <= raio):
        centro[1] = centro[1] - raio
    vizinhos = imagem[centro[0]-raio:centro[0]+raio+1, centro[1]-raio:centro[1]+raio+1]

    maxDegree = 0

    if( vizinhos.shape[0] * vizinhos.shape[1] > 0):
        vPositions = np.where(vizinhos >= 0)

        L = np.amax(vizinhos)
        if L == 0:
            L = 1
        numPixeisVizinhanca = vizinhos.shape[0] * vizinhos.shape[1]
       
        for j in range(0,numPixeisVizinhanca):
            #calcula a distância do pixel atual para os outros
            distanciaX = (vPositions[0][j] - vPositions[0][:])
            distanciaX = distanciaX * distanciaX
            distanciaY = (vPositions[1][j] - vPositions[1][:])
            distanciaY = distanciaY * distanciaY
            somaXY = distanciaX + distanciaY
            distanciaEuclidiana = np.sqrt(somaXY)
 
            maxL = np.max
            difNiveisDeIntensidade = abs(vizinhos[vPositions[0][j]][vPositions[1][j]] - vizinhos[vPositions])
            difNiveisDeIntensidade = difNiveisDeIntensidade / L
            difNiveisDeIntensidade = (raio*raio) * difNiveisDeIntensidade
            
            pesoAresta = distanciaX + distanciaY + difNiveisDeIntensidade
            pesoAresta = pesoAresta/ ((raio * raio) * (raio * raio) )

            adjacentes = np.where( distanciaEuclidiana <= raio )
            maxDegree = max(maxDegree, np.size(adjacentes))
            grafo[j] = [[adjacentes],[pesoAresta[adjacentes]]]
    if(grafo == {}):
        print("ERRO AO MONTAR GRAFO")
    return grafo, maxDegree+1
#### END GENERATE COMPLEX NETWORK 


#### BEGIN GENERATE THE SIGNATURE (FEATURE VECTOR) THAT WILL REPRESENT THE IMAGE ####

def geraConjuntoDePalavrasVisuais(imagem,numeroDeSuperPixels):

    if(SPB == 1):
        segmentos = geraSuperPixels(imagem,numeroDeSuperPixels,0)
    elif(SPB == 2):
        segmentos = geraBlocos(imagem,numeroDeSuperPixels)

    conjuntoDePalavrasVisuais = []
    
    if (MODO == 1):
        
        imagem = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
        #imagem = cv2.cvtColor(imagem, cv2.COLOR_BGR2HSV)
        #imagem = imagem[:,:,0]
        imagem = local_binary_pattern(imagem, 8, 1, method = 'ror').astype(int)
        smin=0; smax=255
        a = np.amin(imagem)
        b = np.amax(imagem)
        imagem = (( imagem - a ) * (smax )) / ( b -a ) 
        
        for superPixel in np.unique(segmentos):
            palavraVisual = geraPalavraVisualRedeComplexa2(superPixel,segmentos,imagem)
            conjuntoDePalavrasVisuais.append(palavraVisual)

    if( MODO == 2):
        imagem = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
        '''imagem = local_binary_pattern(imagem, 8, 1, method = 'ror').astype(int)
        smin=0; smax=255
        a = np.amin(imagem)
        b = np.amax(imagem)
        imagem = (( imagem - a ) * (smax )) / ( b -a )'''
        
        for superPixel in np.unique(segmentos):
            palavraVisual = geraPalavraVisualEstatisticaHist(superPixel,segmentos,imagem)
            conjuntoDePalavrasVisuais.append(palavraVisual)
    return conjuntoDePalavrasVisuais

def estaDentro(elemento,lista):
    for x in lista:
        if (elemento == x):
            return True
    return False

def unicosLento(lista):
    unicos = []
    for x in lista:
        if(not estaDentro(x,unicos)):
            unicos.append(x)
    return unicos

def contaOcorrencias(x,lista):
    contador = 0
    for item in lista:
        if(x == item):
            contador+=1
    return contador

def calculaDistancia(lista1,lista2):
    index = 0
    dif = 0
    while(index < len(lista1)):
        dif+= abs(lista1[index] - lista2[index])
        index+=1
    return dif

def geraRepresentacaoImagens(imagens):
    #lê imagem de busca:
    print("Digite o nome da imagem de busca")
    filename = str(input()).rstrip()
    imagemDeBusca = cv2.imread(diretorio+filename)

    vocabulario = []
    #enerates all visual words in the query image
    conjuntoDePalavrasVisuaisImgBusca = geraConjuntoDePalavrasVisuais(imagemDeBusca,NSP)
    

    #generates all visual words for all images in the database
    conjuntoDePalavrasVisuaisImgsBaseDeDados = []
    for img in imagens:
        conjunto = geraConjuntoDePalavrasVisuais(img[0],NSP)
        conjuntoDePalavrasVisuaisImgsBaseDeDados.append([conjunto,img[1]])
        vocabulario+=conjunto

    vocabulario+=(conjuntoDePalavrasVisuaisImgBusca)
    vocabulario = unicosLento(vocabulario)

    # generates a vector that represents the search image
    representacaoImagemBusca = []
    for item in vocabulario:
        contador = contaOcorrencias(item,conjuntoDePalavrasVisuaisImgBusca)
        representacaoImagemBusca.append(contador)

    representacaoImagensDaBase = []
    representacaoImagensDaBase.append([representacaoImagemBusca,filename])
    # generates a vector that represents each image in the database
    for conjuntoDePalavras in conjuntoDePalavrasVisuaisImgsBaseDeDados:
        representacao = []
        
        for item in vocabulario:
            contador = contaOcorrencias(item,conjuntoDePalavras[0])
            representacao.append(contador)

        representacaoImagensDaBase.append([representacao,conjuntoDePalavras[1]])

    return representacaoImagensDaBase

def leImagens():
    images = []
    for filename in glob.glob( diretorio+"*.jpg"): 
        #print(filename)
        imagem = cv2.imread(filename)
        #imagem = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)
        images.append([imagem,filename[10:]])
    return images 

def calculaDistanciaEntreImagens(representacoes):
    #print(representacoes)
    imgConsulta = representacoes[0]
    distancias = []
    for i in range(1,len(representacoes)):
        distancia = calculaDistancia(imgConsulta[0],representacoes[i][0])
        distancias.append([distancia,representacoes[i][1]])
    return distancias

def cbir():
    imagens=leImagens()
    #search image at position zero / rest of images position 1 forward
    representacaoTodasImagens = geraRepresentacaoImagens(imagens)
    print(len(representacaoTodasImagens[0][0]))
    #calculates distance between all images in the database and the query image
    distancias = calculaDistanciaEntreImagens(representacaoTodasImagens)
    distancias.sort(key = lambda x:x[0])
    for i in range(22):
        print(distancias[i])
    contador = 1
    for item in distancias:
        nomeImagem = diretorio+item[1]
        imagem = cv2.imread(nomeImagem)
        imagem = cv2.cvtColor(imagem, cv2.COLOR_BGR2RGB)
        plt.subplot(3,3,contador)
        plt.imshow(imagem)
        if(contador == 1):
            plt.title("Imagem de Consulta")
        else:
            plt.title("Resultado "+str(contador-1))
        plt.axis('off')
        contador+=1
        if(contador > 9 ):
            break
    plt.show()


    #print(distancias)
#### END GENERATE THE SIGNATURE (FEATURE VECTOR) THAT WILL REPRESENT THE IMAGE ####


#run the code
print("Enter 1 to use SUPERPIXELS or 2 to use BLOCKS")
SPB = int(input())

print("Enter 1 to use Complex Networks or 2 to use MEAN and ENTROPY of the histogram ")
MODO = int(input())

cbir()
